export const SECRETKEY = ''
export const INTERNAL_SERVER = 'Internal server error plese try again after some time';

export const serverError = (res, message, err) => {
    console.log(err);
    return res.status(500).json({
        message: message ? message : INTERNAL_SERVER
    });
}

export const badRequest = (res, message) => {
    return res.status(400).json({ message });
}
